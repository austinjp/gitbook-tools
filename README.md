# Gitbook tools

Linux command-line tools for working with local [GitBook](https://gitbook.com) repositories.

## Requirements

- bash
- Perl (because I'm lazy)
- GitBook
- Node (GitBook requires this)
- ebook-convert (if you want to generate Word .docx files)

## Start/install

Make a new git repository for a new book:

```bash
mkdir my-new-book
cd my-new-book
```

Initialise it as a git repo (after first creating this repo with your host of choice):

```bash
git init
wget -O '.gitignore' 'https://www.gitignore.io/api/git,linux,emacs,gitbook'
git add .
git remote add origin git@bitbucket.org:username/my-new-book.git
git push -u origin master
```

Include this repo as a submodule:

```bash
git submodule add git@bitbucket.org:austinjp/gitbook-tools.git .gitbook-tools
git add .gitmodules .gitbook-tools
git commit -m 'Added austinjp:gitbook-tools submodule'
git push # now or later
# Useful links
export PATH=$PATH":$(pwd)/.gitbook-tools"
```

## Keeping submodule up to date

To ensure you are using the latest version of this submodule:

```bash
# git submodule init # only necessary immediately after cloning a repo that uses this submodule
git submodule update --recursive --remote -- .gitbook-tools
git add .gitbook-tools/
git commit -m 'Latest version'
git push
```
