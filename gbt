#!/bin/bash

set -euo pipefail
thisdir=$(dirname $(readlink -f $0));
projectdir=$(dirname $thisdir)
bookdir=$projectdir"/_book"
srcdir=$projectdir"/_src"
onepagesrcdir=$srcdir"_onepage"
onepagedestdir=$bookdir"_onepage"
project=$(basename $projectdir)

function gitbook_build_web() {
    gitbook build $srcdir $bookdir/
}

function gitbook_build_pdf() {
    gitbook pdf $srcdir $projectdir/$project.pdf
}

function gitbook_build_ebook() {
    gitbook epub $srcdir $projectdir/$project.epub
}

function copy_to_s3() {
    declare -n ret=$1
    local bd=$1
    local bucket=$2
    local s3dir=$3
    local _final='aws s3 cp '"$projectdir"'/'"$bd"' s3://'"$bucket"'/'"$s3dir"'/'
    echo $_final
    ret=$_final
}

function init_gitbook_project() {
    mkdir -p $bookdir
    mkdir -p $srcdir
    mkdir -p $srcdir"/styles/"
    touch $srcdir"/styles/website.css"
    touch $srcdir"/styles/pdf.css"
    touch $srcdir"/styles/epub.css"
    if [ ! -f $srcdir"/book.json"  ]; then
        touch $srcdir"/book.json"
        cat <<'EOF' > $srcdir"/book.json"
{
    "plugins": [
        "-sharing",
        "-highlight",
        "-comment",
        "-lunr"
    ],
    "styles": {
        "website": "styles/website.css",
        "pdf": "styles/pdf.css",
        "epub": "styles/epub.css"
    }
}
EOF
    fi
    gitbook init $srcdir
}

function build_all() {
    gitbook_build_web
    gitbook_build_pdf
    gitbook_build_ebook
}

function onepage_copy_files() {
    mkdir -p $onepagesrcdir
    mkdir -p $onepagedestdir

    pushd $onepagesrcdir 1>/dev/null
    ln -sf $srcdir"/book.json" .
    if [ -f $srcdir"/node_modules" ]; then
        ln -sf $srcdir"/node_modules" .
    fi
    popd 1>/dev/null

    touch $onepagesrcdir"/.bookignore"
    echo > $onepagesrcdir"/.bookignore"
    pushd $srcdir 1>/dev/null
    for d in $(find ./ -type d -name "*" \
                    ! -path "**/node_modules/*" \
                    ! -path "./node_modules/*" \
                    ! -path "./node_modules" \
                    ! -path "**/.git/*" \
                    ! -path "./.git/*" \
                    ! -path "./.git" \
                    ! -path "./"); do
        local mkd=$onepagesrcdir"/$d"
        mkdir -p $mkd
    #     echo $d | perl -ne 's/^\.\///;print' >> $onepagesrcdir"/.bookignore"
    done
    find ./ -type f \
         ! -path "**/node_modules/*" \
         ! -path "./node_modules/*" \
         ! -path "./node_modules" \
         ! -path "**/.git/*" \
         ! -path "./.git/*" \
         ! -path "./.git" \
         ! -iname "book.json" \
         -exec cp -f '{}' $onepagesrcdir/'{}' \;
    popd 1>/dev/null
}

function onepage_make_index() {
    declare -n ret=$1
    tf=$(mktemp -p $onepagesrcdir)
    mv -f $tf $tf".md"
    tf=$tf".md"
    for i in $(grep -E '[\[|\(]' $onepagesrcdir"/SUMMARY.md" | \
                      perl -ne 's/^.*\((.+)\).*$/$1/msg; s/^\#.*$//msg; s/\s+/ /msg; print $_."\n" if($_);'); do
        echo '{% include "'$i'" %}' >> $tf
    done
    ret=$tf
}

function onepage_prep() {
    mkdir -p $onepagesrcdir
}

function onepage_web_gitbook_build() {
    gitbook build $onepagesrcdir $onepagedestdir
}

function onepage_append_link_to_summary() {
    local tf=$1
    tf=$(basename $tf)
    touch $onepagesrcdir"/SUMMARY.md"
    echo >> $onepagesrcdir"/SUMMARY.md"
    echo "---" >> $onepagesrcdir"/SUMMARY.md"
    echo "* [One page]("$tf")" >> $onepagesrcdir"/SUMMARY.md"
    echo >> $onepagesrcdir"/SUMMARY.md"
}

function onepage_swap_index() {
    local tf=$1
    tf=$(basename $tf)
    tf=$(echo $tf | sed 's/\.md$/.html/')
    cp -f $onepagedestdir"/index.html" $onepagedestdir"/index.html.bak"
    mv -f $onepagedestdir"/"$tf $onepagedestdir"/index.html"
}

function onepage_tidy_html() {
    local tf=$1
    tf=$(basename $tf)
    tf=$(echo $tf | sed 's/\.md$/.html/')
    sed -i'' "s/$tf/.\//" $onepagedestdir"/index.html"
}

function onepage_web() {
    local tempfile
    onepage_prep
    onepage_copy_files
    onepage_make_index tempfile
    onepage_append_link_to_summary "$tempfile"
    onepage_web_gitbook_build
    onepage_swap_index "$tempfile"
    onepage_tidy_html "$tempfile"
}

function display_usage() {
cat <<EOF
gbt - command-line tools for making it easier to work with GitBook.

Usage:  gbt [option]
        gbt s3 [options]

Options:

    init:
        Create _src/ and _book/ dir for source and build files. GitBook
        will read from _src/ and build into _book/ directories. A simple
        book.json is also created in _src/

    build-web:
        Runs 'gitbook build' on _src/ writing files into _book/ suitable
        for serving as web pages.

    onepage-web:
        Copies files from _src/ into _src_onepage/ creating that dir if
        does not exist. Then runs 'gitbook build' on _src_onepage/ with
        build files written into_book_onepage/
        Subsequently this does some fiddling around (with mixed success)
        with files in order to end up with a single file in
        _book_onepage/index.html

    s3:
        Convenience method to run 'aws s3 cp <files> s3://<bucket>/<dir>'
        Parameters are expected in that order, e.g.:
        gbt s3 _book my_bucket dir/subdir/subsubdir
EOF
}

if [ "$#" -eq 0 ]
then
    display_usage
    exit 1
fi

case "init"
in
    $1)
        init_gitbook_project
        exit
        ;;
esac

case "s3"
in
    $1)
        copy_to_s3 $2 $3 $4
        exit
        ;;
esac

case "build-web"
in
    $1)
        gitbook_build_web
        exit
        ;;
esac

case "build-pdf"
in
    $1)
        gitbook_build_pdf
        exit
        ;;
esac

case "build-ebook"
in
    $1)
        gitbook_build_ebook
        exit
        ;;
esac

case "build-all"
in
    $1)
        build_all
        exit
        ;;
esac

case "onepage-web"
in
    $1)
        onepage_web
        exit
        ;;
esac

display_usage
